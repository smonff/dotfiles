```
      ██            ██     ████ ██  ██
     ░██           ░██    ░██░ ░░  ░██
     ░██  ██████  ██████ ██████ ██ ░██  █████   ██████
  ██████ ██░░░░██░░░██░ ░░░██░ ░██ ░██ ██░░░██ ██░░░░
 ██░░░██░██   ░██  ░██    ░██  ░██ ░██░███████░░█████
░██  ░██░██   ░██  ░██    ░██  ░██ ░██░██░░░░  ░░░░░██
░░██████░░██████   ░░██   ░██  ░██ ███░░██████ ██████
 ░░░░░░  ░░░░░░     ░░    ░░   ░░ ░░░  ░░░░░░ ░░░░░░

  ▓▓▓▓▓▓▓▓▓▓
 ░▓ about  ▓ custom gnu/linux config files managed with YADM
 ░▓ author ▓ smonff <smonff@riseup.net>
 ░▓ code   ▓ https://framagit.org/smonff/dotfiles
 ░▓▓▓▓▓▓▓▓▓▓
 ░░░░░░░░░░

 awesome        > awesome wm config
 bash           > bash shell settings, aliases, custom prompts and dirty things
 emacs          > configuration made around the Prelude distribution
 tmux           > terminal multiplexer with custom status bar
 linters        > various code formaters and linters
```

# Table of contents
 - [introduction](#dotfiles)
 - [managing](#managing)
 - [how it works](#how-it-works)
 - [previews](#previews)
 - [credits](#credits)

# Dotfiles

In the Unix world programs are commonly configured in two different
ways, via shell arguments or text based configuration files. Programs
with many options like window managers or text editors are configured
on a per-user basis with files in your home directory `~`. In unix
like operating systems any file or directory name that starts with a
period or full stop character is considered hidden, and in a default
view will not be displayed. Thus the name dotfiles.

It's been said of every console user:

    > _"you are your dotfiles"_.

It looks a bit like stupid fetishism though.

# Managing
This repository contain *dotfiles* (there is also `rc` files) that are
managed by [YADM](https://thelocehiliosan.github.io/yadm/) (Yet
Another Dotfile Manager). Let's YADM present themselves:

> When you live in a command line, configurations are a deeply personal
thing. They are often crafted over years of experience, battles lost,
lessons learned, advice followed, and ingenuity rewarded. When you are
away from your own configurations, you are an orphaned refugee in
unfamiliar and hostile surroundings. You feel clumsy and out of
sorts. You are filled with a sense of longing to be back in a place
you know. A place you built. A place where all the short-cuts have
been worn bare by your own travels. A place you proudly call… $HOME.

After installing YADM and initializing it, these files will not be
deployed in the `~/.yalm/` repository, but where they are supposed to
be stored.

This is my personal backup and deployment tool, but feel free to study
or reuse it if it can be useful.

# Credits
This README structure is based on
[one written by xero](https://github.com/xero/dotfiles/blob/master/README.md)
for his own dot files. The content is very different and the method too.
