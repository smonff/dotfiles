;;; custom.el --- My own stuff

;;
;; Copyright (c) 2016 Sébastien Feugère
;;
;; Author: Sébastien Feugère <smonff@riseup.net>
;; URL:
;; Version: 0.0.2
;; Keywords: convenience

;; This file is not part of GNU Emacs.

;;; Commentary:

;;; UI beautiful stuff and auto saved preferences

;; This file simply sets up the default load path and requires
;; the various modules defined within Emacs Prelude.

;;; License:

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; as published by the Free Software Foundation; either version 3
;; of the License, or (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
;; Boston, MA 02110-1301, USA.

;;; Code:

;;;;
;; Customization
;;
;; To reload easily the customization without restarting Emacs, use
;; the following command in the scratchpad
;;
;;     (load "~/.emacs.d/init.el") ;; and execute it with ctrl + j
;;
;; ...or M-x reload-config !
;;;

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path "~/.emacs.d/personal/custom/")

(load "navigation.el")
(load "ui.el")
(load "package.el")
(load "editing.el" )
(load "sys.el")
(load "util.el")

;;; Language-specific
(add-to-list 'load-path "~/.emacs.d/personal/lang/")

(load "css.el")
(load "git.el")
;; (load "js.el")
(load "markdown.el")
(load "tex.el")
(load "perl6.el")
;; (load "elisp-editing.el")
;; (load "setup-clojure.el")
;; (load "setup-perl.el")
;; (load "setup-java.el")
;; (load "setup-web.el")


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-enabled-themes (quote (apropospriate-dark)))
 '(custom-safe-themes
   (quote
    ("5a0eee1070a4fc64268f008a4c7abfda32d912118e080e18c3c865ef864d1bea" "c3e6b52caa77cb09c049d3c973798bc64b5c43cc437d449eacf35b3e776bf85c" default)))
 '(fci-rule-color "#383838")
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(package-selected-packages
   (quote
    (charmap company-tern less-css-mode no-easy-keys pkgbuild-mode syslog-mode wget company use-package yaml-mode xref-js2 xah-find websocket web-mode unfill tt-mode theme-changer test-simple tern-auto-complete sudo-edit solarized-theme smex smart-mode-line-powerline-theme slime scss-mode sclang-snippets sclang-extensions rust-mode react-snippets rainbow-delimiters rainbow-mode pug-mode php-mode persistent-soft perl6-mode pdf-tools nodejs-repl markdown-mode magit-gitflow lua-mode keychain-environment json-mode js2-refactor js-comint jabber ido-vertical-mode ido-ubiquitous helm-projectile gulp-task-runner go-mode geiser flycheck-pos-tip flycheck-perl6 flx-ido elisp-slime-nav drag-stuff discover csv-mode aurel apropospriate-theme ac-js2 2048-game pod-mode graphviz-dot-mode zop-to-char zenburn-theme which-key volatile-highlights undo-tree smartrep smartparens smart-mode-line operate-on-number move-text magit projectile ov imenu-anywhere guru-mode grizzl god-mode gitignore-mode gitconfig-mode git-timemachine gist flycheck expand-region epl editorconfig easy-kill diminish diff-hl discover-my-major dash crux browse-kill-ring beacon anzu ace-window)))
 '(pdf-view-midnight-colors (quote ("#DCDCCC" . "#383838")))
 '(safe-local-variable-values (quote ((flycheck-disabled-checkers emacs-lisp-checkdoc))))
 '(vc-annotate-background "#2B2B2B")
 '(vc-annotate-color-map
   (quote
    ((20 . "#BC8383")
     (40 . "#CC9393")
     (60 . "#DFAF8F")
     (80 . "#D0BF8F")
     (100 . "#E0CF9F")
     (120 . "#F0DFAF")
     (140 . "#5F7F5F")
     (160 . "#7F9F7F")
     (180 . "#8FB28F")
     (200 . "#9FC59F")
     (220 . "#AFD8AF")
     (240 . "#BFEBBF")
     (260 . "#93E0E3")
     (280 . "#6CA0A3")
     (300 . "#7CB8BB")
     (320 . "#8CD0D3")
     (340 . "#94BFF3")
     (360 . "#DC8CC3"))))
 '(vc-annotate-very-old-color "#DC8CC3"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
