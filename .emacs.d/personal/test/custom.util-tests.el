;; These tests can be run with a simple C-c C-b (eval-current-buffer)

(require 'test-simple)
(test-simple-start "custom.utils-tests.el")

(assert-t (load-file  "../custom/util.el") "Can't load util.el")
(assert-t (reload-config) "Config can't be reloaded")
(assert-nil (ispell-turn-to-french) "French FlySpell hasn't been loaded")
(assert-nil (emacs-open-config) "open init.el feature is not implemented")

(end-tests)
