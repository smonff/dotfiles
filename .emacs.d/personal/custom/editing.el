;;; editing.el ---
;;

;;; Commentary:
;;

;;; Code:


;; Autofill everywhere by default sister because we like these
;; sentences to be cutted  automatically
;; Potentially buggy on source code
;; (setq-default auto-fill-function 'do-auto-fill)

;; Move line or selected text
;; Drag stuff with M-<up>, M-<down>, M-n, M-p, etc.
;; https://github.com/rejeep/drag-stuff.el
(require 'drag-stuff)
(drag-stuff-global-mode 1)
(drag-stuff-define-keys)

;; Global company-mode is enabled by Prelude
;; Allready activated in prelude-modules.el
;; (global-company-mode)
;; (setq company-backends '((company-capf company-files company-dabbre)))

;; Display typographic invisible chars (spaces, tab, newline)
;; (add-to-list 'whitespace-style 'space-mark)
;; (add-to-list 'whitespace-style 'tab-mark)
;; (add-to-list 'whitespace-style 'newline-mark)
;; (setq whitespace-display-mappings
;; '(
;; (newline-mark ?\n [?\u00B6 ?\n])))

;; Display a nice char counter in modline
(smart-mode-line-enable 1)

;; Handy key definition for unfill-mode
(global-set-key (kbd "<f8>") 'unfill-toggle)


;;; editing.el ends here
