;; ido-mode allows you to more easily navigate choices. For example,
;; when you want to switch buffers, ido presents you with a list
;; of buffers in the the mini-buffer. As you start to type a buffer's
;; name, ido will narrow down the list of buffers to match the text
;; you've typed in
;; http://www.emacswiki.org/emacs/InteractivelyDoThings

;;; Code:
(require 'ido)
(ido-mode t)

;; This allows partial matches, e.g. "tl" will match "Tyrion Lannister"
(setq ido-enable-flex-matching t)

;; Turn this behavior off because it's annoying
(setq ido-use-filename-at-point nil)

;; Don't try to match file across all "work" directories; only match files
;; in the current directory displayed in the minibuffer
(setq ido-auto-merge-work-directories-length -1)

;; Includes buffer names of recently open files, even if they're not
;; open now
(setq ido-use-virtual-buffers t)

;; This enables ido in all contexts where it could be useful, not just
;; for selecting buffer and file names
(ido-ubiquitous-mode 1)

;; Displays Ido vertically
(require 'ido-vertical-mode)
(ido-vertical-mode 1)
(setq ido-vertical-define-keys 'C-n-and-C-p-only)

(projectile-mode)
(global-set-key (kbd "C-<tab>") 'projectile-recentf)

;; Inhibit the keys as arrow to make more easy to learn the "right
;; way to use Emacs"
;; Very good way to learn, but it really create some tired eyes for
;; people who can't touch-type
;;(require 'no-easy-keys)
;;(no-easy-keys 1)

;; Some navigation keybindings that needed to be redefined
(global-set-key (kbd "C-x .") 'next-buffer)
(global-set-key (kbd "C-x <right>") 'ignore)
(global-set-key (kbd "C-x ,") 'previous-buffer)
(global-set-key (kbd "C-x <left>") 'ignore)
(global-set-key (kbd "M-[") 'backward-paragraph)
(global-set-key (kbd "M-]") 'forward-paragraph)

;; Smex is a M-x enhancement for Emacs, it provides a convenient
;; interface to your recently and most frequently used commands.
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(setq smex-prompt-string "M-x ")

;; Remap Company's autocompletion menu navigation keybindings
;; https://emacs.stackexchange.com/questions/2988/
(with-eval-after-load 'company
  (define-key company-active-map (kbd "M-n") nil)
  (define-key company-active-map (kbd "M-p") nil)
  (define-key company-active-map (kbd "C-n") #'company-select-next)
  (define-key company-active-map (kbd "C-p") #'company-select-previous))

;; Remap AutoComplete's autocompletion menu navigation keybindings
;; Cycle candidates with C-n and C-p (the remaping is done in auto-complete.el)
(setq ac-use-menu-map t)

;; Scroll the page with in-place point
(defun gcm-scroll-down ()
  (interactive)
  (scroll-up 1))

(defun gcm-scroll-up ()
  (interactive)
  (scroll-down 1))

(global-set-key (kbd "M-\\") 'gcm-scroll-up)
(global-set-key (kbd "C-\\") 'gcm-scroll-down)

;; For multiples cursors by mouse
(global-unset-key (kbd "M-<down-mouse-1>"))
(global-set-key (kbd "M-<mouse-1>") 'mc/add-cursor-on-click)
(global-set-key (kbd "C-x C-l") 'mc/edit-lines)
(global-set-key (kbd "<f7>") 'mc/mark-pop)

;; Enable ergoemacs mode
;; (setq ergoemacs-theme nil) ;; Uses Standard Ergoemacs keyboard theme
;; (setq ergoemacs-keyboard-layout "us") ;; Assumes QWERTY keyboard layout
;; (ergoemacs-mode 1)

;; Handy shortcut for changing buffer without C-x o
(global-set-key (kbd "<f9>") 'other-window)
(global-set-key (kbd "<M-tab>") 'other-window)

;;; navigation.el ends here
