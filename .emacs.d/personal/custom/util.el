;;; util.el --- Very handy shortcuts

;;; Commentary:

;;; Code:

;; Makes easy to reload Emacs configuration without having to reboot
;; or anything
(defun reload-config ()
  "Reload your Emacs configuration without restarting it."
  (interactive)
  (load-file "~/.emacs.d/init.el"))

;; Turn a two steps command in a one step command
(defun ispell-turn-to-french ()
  "Two commands in only one for loading the French Ispell dictionary."
  (interactive)
  (ispell-change-dictionary "fr_FR"))

;; Open configuration directory
(defun emacs-open-config ()
  "Open the Prelude personal directory in Dired."
  (interactive)
  (find-file (print default-directory)))

;; Save the state of Emacs from one session to another
(desktop-save-mode 1)

;; pdf-tools
(require 'pdf-tools)
(add-to-list 'auto-mode-alist '("\\.pdf\\'" . pdf-view-mode))

;;; util.el ends here
