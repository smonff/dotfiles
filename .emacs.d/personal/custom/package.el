;;; package.el --- Defines custom packages repositories
;;

;;; Commentary:
;;

;;; Code:

(add-to-list 'package-archives
             '("marmelade" . "https://marmalade-repo.org/packages/") t)

;; Packages that we want on next installation
;; Require the Marmelade repo
(prelude-require-packages
 '(
   company
   ido-vertical-mode
   solarized-theme
   sudo-edit
   theme-changer
   ;;pod-mode
   zop-to-char
   zenburn-theme
   which-key
   ;;wget
   volatile-highlights
   ;;syslog-mode
   sudo-edit
   solarized-theme
   smex
   smartrep
   smartparens
   pkgbuild-mode
   ov
   no-easy-keys
   move-text
   less-css-mode
   imenu-anywhere
   guru-mode
   grizzl
   god-mode
   gitignore-mode
   gitconfig-mode
   git-timemachine
   gist
   expand-region
   easy-kill
   discover-my-major
   diminish
   diff-hl
   crux
   company-tern
   charmap
   browse-kill-ring
   beacon
   anzu
   ace-window))

;;; package.el ends here
