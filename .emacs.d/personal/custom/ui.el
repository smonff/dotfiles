;;; ui.el --- Provides user interface customizations
;;

;;; Commentary:
;;

;;; Code:

;; We use an automatic module that chooses between light and dark
;; theme in function of the day time

;; Set the location
;; (setq calendar-longitude-name "Paris, FR")
;; (setq calendar-latitude 48.80743)
;; (setq calendar-longitude 2.33469)

;; Specify the day and night themes
;; (require 'theme-changer)
(require 'apropospriate)
;;(change-theme 'apropospriate-light 'apropospriate-dark)
 ;; (change-theme 'solarized-light 'solarized-dark)
(load-theme 'apropospriate-light t)


(defun switch-to-solarized-light ()
  "Switch from night to day."
  (interactive)
  (load-theme 'solarized-light))

(defun switch-to-solarized-dark ()
  "Switch from day to dark."
  (interactive)
  (load-theme 'solarized-dark))

(defun switch-to-apropospriate-light ()
  "Switch from night to day."
  (interactive)
  (load-theme 'apropospriate-light))

(defun switch-to-dracula ()
  "Switch to Dracula theme."
  (interactive)
  (load-theme 'dracula))

;; Don't show native OS scroll bars for buffers because they're redundant
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode 0))

;; Modeline colors
;;(setq sml/theme 'respectful)
;;(sml/setup)

;; The fringe, with a large one on the left and a small one on the right
(fringe-mode '(30 . 5))

;; A modernized mode-line or status bar for emacs
;; (require 'ergoemacs-status)
;; (ergoemacs-status-mode)
;;; ui.el ends here
