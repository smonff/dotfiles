;; git.el --- Specific Git customizations
;;

;;; Commentary:
;;

;;; Code:

;; Magit
;; Would like to disable whitespace-mode in Magit commit message buffer

;; Gitflow
;;; C-f in the magit status buffer invokes the magit-gitflow popup. If you
;;; would like to use a different key, set the magit-gitflow-popup-key variable
;;; before loading magit-gitflow
;; (setq magit-gitflow-popup-key "C-n")

(require 'magit-gitflow)
(add-hook 'magit-mode-hook 'turn-on-magit-gitflow)

;;; git.el ends here
