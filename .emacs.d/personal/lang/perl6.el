;; perl6.el --- Specific Perl6 customizations
;;

;;; Commentary:
;;

;;; Code:


(use-package flycheck
  :ensure t
  :defer t
  :init (add-hook 'prog-mode-hook 'flycheck-mode)
  :config
  (use-package flycheck-perl6
    :ensure t))

;;; perl6.el ends here
