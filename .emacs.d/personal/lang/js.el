;; js.el --- Specific JavaScript customizations
;;

;;; Commentary:
;;

;;; Code:

;; CAUTION web-beautify is horribly bugged, especially with web-mode Wed 01 Nov 2017 02:18:54 PM CET
;; Automatically beautify JS files the right way on save
;; (eval-after-load 'js2-mode
;;   '(add-hook 'js2-mode-hook
;;              (lambda ()
;;                (add-hook 'before-save-hook 'web-beautify-js-buffer t))))
;;                                                                  ;; a
                                                                 ;; second t ?


;; This is a list of functions that you don't want ESlint to consider
;; as known
;; (setq-default js2-global-externs '("$" "console" "stringify"))

;; Autocompletion and API documentation
;; API documentation can be refresh with (jquery-doc-fetch-and-generate-data)
;; (require 'jquery-doc)
;; (add-hook 'js2-mode-hook 'jquery-doc-setup)

;; (require 'js2-mode)
;; (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

;; 2 spaces indentation for JSON files
(add-hook 'json-mode-hook
          (lambda ()
            (make-local-variable 'js-indent-level)
            (setq js-indent-level 2)))

;; Better imenu
;; (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)

;;(require 'js2-refactor)
;; (require 'xref-js2)

;;(add-hook 'js2-mode-hook #'js2-refactor-mode)
;; (js2r-add-keybindings-with-prefix "C-c C-r")
;; (define-key js2-mode-map (kbd "C-k") #'js2r-kill)

;; js-mode (which js2 is based on) binds "M-." which conflicts with xref, so
;; unbind it.
;; (define-key js-mode-map (kbd "M-.") nil)

;; (add-hook 'js2-mode-hook (lambda ()
;;   (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))

;; (add-to-list 'load-path "/usr/lib/node_modules/tern/emacs/")
;; (autoload 'tern-mode "tern.el" nil t)

;; (require 'company)
;; (require 'company-tern)
;; (add-to-list 'company-backends 'company-tern)

;; (add-hook 'js2-mode-hook (lambda ()
;;                            (tern-mode t)
;;                            (company-mode t)))

;; Company
;; (add-to-list 'company-backends 'company-tern)
;; (add-hook 'js2-mode-hook ()
;;            (company-mode)
;;            (tern-mode t))

;; To make tern work, it is necessary to activate auto-complete-mode
;; manually !!!!!
;; M-x auto-complet-mode
;; (eval-after-load 'tern
;;    '(progn
;;        (require 'tern-auto-complete)
;;        (tern-ac-setup)))

;; js2-refactor
;; (require 'js2-refactor)
;; (add-hook 'js2-mode-hook #'js2-refactor-mode)
;; (js2r-add-keybindings-with-prefix "C-c C-m")

;; nodejs-repl
;; (add-hook 'js-mode-hook
;;           (lambda ()
;;             (define-key js-mode-map (kbd "C-x C-e") 'nodejs-repl-send-last-sexp)
;;             (define-key js-mode-map (kbd "C-c C-r") 'nodejs-repl-send-region)
;;             (define-key js-mode-map (kbd "C-c C-l") 'nodejs-repl-load-file)
;;             (define-key js-mode-map (kbd "C-c C-z") 'nodejs-repl-switch-to-repl)))

;; Restart tern server
;; (defun delete-tern-process ()
;;   (interactive)
;;   (delete-process "Tern"))


;;; js.el ends here
