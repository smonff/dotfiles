;; tex.el --- Specific LaTex customizations
;;

;;; Commentary:
;;

;;; Code:

;; Automatically turn off auto-fill mode
(add-hook 'tex-mode-hook (lambda () (auto-fill-mode -1)))

;; Automatically turn off whitespace mode
;;(setq whitespace-global-modes '(not tex-mode))
(add-hook 'tex-mode-hook (lambda () (whitespace-mode -1)))


;;; tex.el ends here
