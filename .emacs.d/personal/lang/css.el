;; css.el --- Specific CSS customizations
;;

;;; Commentary:
;;

;;; Code:

'(css-indent-offset 4)

;; Automatically beautify CSS files the right way on save
;; JS BEAUTIFY IS ACTUALLY BUGGED Wed 01 Nov 2017 02:18:02 PM CET
;; NOTE; If you want to config web-beautify, you'll need a
;; .jsbeautifyrc file at the root of your project. The content must be
;; valid JSON, see https://github.com/yasuyk/web-beautify#user-content-jsbeautifyrc-files
;; (eval-after-load 'css-mode
;;   '(add-hook 'css-mode-hook
;;              (lambda ()
;;                (add-hook 'before-save-hook 'web-beautify-css-buffer t t))))

(setq-default flycheck-error-list-set-level 'error)

;; Automatically enable the errors list in CSS mode
;; DON'T WORK
;; (add-hook 'after-init-hook 'css-mode-hook
;;           (add-hook 'flycheck-mode-hook 'flycheck-list-errors))

;;; css.el ends here
