;; markdown.el --- Specific MarkDown customizations
;;

;;; Commentary:
;;

;;; Code:

(eval-after-load 'css-mode
  ;; Automatically use electric-pair-mode
  (electric-pair-mode 1)

)


;; Autocompletion of stars used by italic
(defvar markdown-electric-pairs '((?* . ?*)) "Electric pairs for markdown-mode.")
(defun markdown-add-electric-pairs ()
  (setq-local electric-pair-pairs (append electric-pair-pairs markdown-electric-pairs))
  (setq-local electric-pair-text-pairs electric-pair-pairs))

(add-hook 'markdown-mode-hook 'markdown-add-electric-pairs)
;;; markdown.el ends here
