if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

xhost +local:root > /dev/null 2>&1

complete -cf sudo

shopt -s cdspell
shopt -s checkwinsize
shopt -s cmdhist
shopt -s dotglob
shopt -s expand_aliases
shopt -s extglob
shopt -s histappend
shopt -s hostcomplete
shopt -s nocaseglob

export HISTSIZE=10000
export HISTFILESIZE=${HISTSIZE}
export HISTCONTROL=ignoreboth

alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -lh --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -lah --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=tty -d skip'
alias cp="cp -i"                          # confirm before overwriting something
alias rm="rm -i"                          # prompt before removing
# Display progress while copying
alias vrsync="rsync -av --progress"
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias np='nano PKGBUILD'
alias mix='alsamixer'

# Manage SSH keys with keychain
# https://wiki.archlinux.org/index.php/Ssh_keys#Keychain
eval $(keychain --eval --agents ssh -Q --quiet id_rsa)

# Enable a better completion
# Very useful for parameters like "systemctl start", it will provide the
# full available list of startable services instead of a list of the
# files in the current directory :D
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] &&
  . /usr/share/bash-completion/bash_completion

# ex - archive extractor
# usage: ex <file>
ex ()
{
  if [ -f $1 ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1 --totals  ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1     ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# In some cases some zip files are "corrupted"
# https://huit.re/MMnBu4uG
recover_archive () {
    jar xvf $1
}


COMPUTER="\342\231\245" # A earth
FANCY="\342\236\234"    # A fancy unicode arrowed prompt
ERROR_CROSS="\342\234\230"

source ~/.config/colors
source /usr/share/git/completion/git-completion.bash
source /usr/share/git/completion/git-prompt.sh
alias sourcebrc='source ~/.bashrc'

# prompt
# PS1="\u@\H \[$Grey\]\w\$(__git_ps1 \" (%s)\") \n\[$BPurple\] $FANCY \[$Color_Off\]"
PROMPT_USERNAME='\u'
PROMPT_HOST='\H'
PROMPT_PATH="\[$Purple\]\w"
PROMPT_GIT="\[$Color_Off\]\$(__git_ps1 \" (%s)\")"
PROMPT_PROMPT="\[$Green\]$FANCY"
PS1="$PROMPT_USERNAME@$PROMPT_HOST $PROMPT_PATH$PROMPT_GIT $PROMPT_PROMPT \[$Color_Off\] "
#PS1='[\u@\h \W]\$ '

export EDITOR='emacs'

export LANG=en_US.utf-8

# TMUX
# 256 colors Tmux
alias tmux='tmux -2'
# This conditional statement ensures that the TERM variable is
# only set outside of tmux, since tmux sets its own terminal
#[ -z "$TMUX" ] && export TERM=xterm-256color
export TERM=xterm-256color
export LM_DEFAULT_COLOR='debug=red:info=;cyan:critical=yellow;red'


# Reload OpenBox conf
alias ropenbox='openbox --reconfigure'
# Reload Tint2 panel bar after a configuration change
alias rtint='killall -SIGUSR1 tint2'
# Subtitles downloader for http://opensubtitles.org (takes the name of
# the movie file as an argument)
alias subdl='subdl --lang=fre,en --interactive'

# Alias for screen brightness + / -
alias li+='light -A 5'
alias li-='light -U 5'
alias lighter='xbacklight -set 100'
alias darker='xbacklight -set 1'

# Also possible to use the  'brightness' graphical application to
# control the brightness of each screen

#export PERL5LIB=/home/smonff/perl5/lib/perl5
# Enable the use of the local::lib perl bootstrap module
#[ $SHLVL -eq 1 ] && eval "$(perl -I$HOME/perl5/lib/perl5 -Mlocal::lib)"

# ncdu is a tool to find big files in your drive

# Sed is your friend
# https://stackoverflow.com/questions/5410757/delete-a-line-containing-a-specific-string-using-sed
# sed -i '/pattern/d' /my/path/*.file

 ##############################################################################
# Command line Perl toolbox                                                    #
#                                                                              #
# pm-uninstall - Uninstall modules                                             #
# module-version (App::module::version) - Gets the version info about a module #
# corelist - a commandline frontend to Module::CoreList                        #
# plister - search inside Perl @INC and find installed module                  #
# modulemaker - ExtUtils::MakeMaker helper for starting                        #
# cpan-uploader - Upload the distribution to PAUSE                             #
# uni - Command-line utility to find or display Unicode characters             #
 ##############################################################################

export PATH=${PATH}:/home/smonff/perl5/bin;
PERL5LIB="/home/smonff/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/smonff/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/smonff/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/smonff/perl5"; export PERL_MM_OPT;

# Perl6 stuff
export PATH=$PATH:/opt/rakudo-star-2017.07/bin:/opt/rakudo-star-2017.07/share/perl6/site/bin:/home/smonff/.perl6/bin
# Note that the comma (',') is used as the directory separator (instead
# of  the colon (':') as with Perl 5 for PERL5LIB or PERLLIB).
# https://docs.perl6.org/language/modules#Exporting_and_Selective_Importing
export PERL6LIB=/home/smonff/project/Art-World/lib,/home/smonff/.perl6

alias p6='perl6'

# -v can be used for a verbose stdout
alias cpanm='cpanm -v'

# Allow to search inside Perl @INC and find installed modules.
# Note that it can take arguments
function plister () {
    # Get the current position to go back into it  after the script run
    INITIAL_POSITION=$(pwd)

    PLISTER_HOME=$HOME/perl5/lib/perl5/App/Module/
    # Have to move because of limitation of the module
    # See http://search.cpan.org/dist/App-Module-Lister/lib/App/Module/Lister.pm#TO_DO
    cd ${PLISTER_HOME}

    # Search with a pattern (from command line argument)
    if [ $1 ] ; then
        # \grep "unalias" grep if you got some
        perl Lister.pm | \grep --ignore-case $1
        echo "Searched with App::Module::Lister with pattern $1"
    # Search all modules installed
    else
        perl Lister.pm
        echo "Searched with App::Module::Lister with no pattern"
    fi

    cd ${INITIAL_POSITION}
}

# Stupid shortcuts for stupid people
alias repo='cd ~/project/'
niniche="/home/smonff/etyssa/src/gityssa/webcontent/"
gfx="/home/smonff/etyssa/gfx/"

# The Perl interpreter !
# Also, a calculator
# perl -plE '$_=eval',

# Calculator with floating point
alias bc='bc -l'

# The great _httpsee shows your Tomcat logs
alias _httpsee='tailf /home/smonff/etyssa/var/log/catalina.out | ccze -A'
#
# alias _httpsee='tailf /home/smonff/etyssa/var/log/catalina.out | ccze -A -o scroll'

# Throw away the Tomcat cache
function minouclean() {
    perl /home/smonff/opt/scripts/minouclean/remove-scriptysse-cache.pl
}

# Remove .orig files - see also git clean
# https://stackoverflow.com/questions/12366150/how-to-delete-orig-files-after-merge-from-git-repository
function gitclean() {
    # To test new patterns, just remove the -delete argument
    find . -name '*.orig' -delete
    find . -name '#*.*#' -delete
    echo "git .orig and emacs backup files removed"
}

alias gitd='git diff'
alias gits='git status -uno'


# Full Arch package upgrade with Aur support
alias sysupgrade='trizen -Su'
# Also upgrade SVN/CVS/HG/GIT package
alias sysupgrade-devel='trizen -Su --devel'

alias mysqlstart='sudo systemctl start mysqld'

# Wake ups Emacs when make it go to sleep with Ctrl + z
alias wakupemacs='fg %emacs'

# List partitions, and then you can umount it
alias disks='lsblk'

alias mist='mist -asset_path="/usr/share/mist"'

export PATH=$PATH:/home/smonff/opt/broken_perl/

# Ethernet aliases
# Simplier to use 'systemctl restart dhcpd'
alias ifdown='sudo ip link set enp3s0 down'
alias ifup='sudo ip link set enp3s0 up'

# Fuckin' wifi
# Just use nm-applet and don't break your balls

alias azerty='setxkbmap fr'
alias qwerty='setxkbmap -layout us -variant intl'

# Archive checker
alias checkar='/home/smonff/project/Archive-Checker/archive_checker.sh'

alias net_restart='systemctl restart dhcpdc'

alias git-stats-importer='git-stats-importer -e smonff@riseup.net,sebastien@etyssa.fr,seb.feugere@gmail.com,smonff@gmail.com'

#alias steam="LD_PRELOAD='/usr/lib/libstdc++.so.6 /usr/lib/libgcc_s.so.1 /usr/lib/libxcb.so.1' steam"

alias steam="LD_PRELOAD=~/.local/share/Steam/ubuntu12_32/steam-runtime/amd64/usr/lib/x86_64-linux-gnu/libSDL2-2.0.so.0 %command% -screen-fullscreen 0 -screen-width 1600 -screen-height 900"


# Ack will be case-unsensitive
#alias ack='ack -i'

alias yaourt='trizen'

function generate_awesome_wm_menu() {
  xdg_menu --format awesome --root-menu /etc/xdg/menus/arch-applications.menu > ~/.config/awesome/xdgmenu.lua
  cat ~/.config/awesome/archmenu.lua
  echo "Menu generated in ~/.config/awesome/"
}

function wttr {
   curl http://wttr.in/$1
}

# ifconfig equivalent
alias ifconfig='ip addr'

# Keep one version of the last package and remove everything else in /var/cache/pacman/
alias clean_pacman_cache='paccache -k 1 -r'

#export JAVA_HOME=/usr/lib/jvm/default-runtime
#export PATH=$PATH:$JAVA_HOME/bin

export GOPATH=~/.go
export PATH=$PATH:~/.go/bin


function toggle_sticky_keys {
    xkbset sticky -twokey -latchlock
}

function keyboard_init {
    qwerty
    echo "Clavier qwerty ok"
    # Voir .xinitrc pour explication
    xkbset accessx sticky -twokey -latchlock
    xkbset exp 1 =accessx =sticky =twokey =latchlock
    echo "Sticky keys ok"
}

# https://github.com/ValveSoftware/steam-runtime/issues/13
function steam_clean {
    find ~/.steam/root/ \( -name "libgcc_s.so*" -o -name "libstdc++.so*" -o -name "libxcb.so*" \) -print -delete
}

# To start Steam, run this
function steam_start {
    env LD_PRELOAD='/usr/$LIB/libstdc++.so.6 /usr/$LIB/libgcc_s.so.1 /usr/$LIB/libxcb.so.1 /usr/$LIB/libgpg-error.so' /usr/bin/steam %U
}

# Handy tool to fine-tune MariaDB
function mysqltuner_easy {
    mysqltuner --user root --pass
}

# Collects information about loadable modules for Gdk Pixbuf and
# writes it to the default cache file location. Useful when you want
# to generate some theme or font cache. GdkPixbuf is a toolkit for
# image loading and pixel buffer manipulation used in GTK+
function update_gdk_pixbuf_cache {
    sudo gdk-pixbuf-query-loaders --update-cache
}

# Shortcut to mount the old system partition
function mount-sda5 {
    sudo mount /dev/sda5 ~/manjaro/
}

function serve {
    python -m http.server 8888
}
