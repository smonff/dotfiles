 local menu98edb85b00d9527ad5acebe451b3fae6 = {
     {"Archive Manager", "file-roller ", "/usr/share/icons/hicolor/16x16/apps/file-roller.png" },
     {"Calculator", "gnome-calculator", "/usr/share/icons/gnome/16x16/apps/accessories-calculator.png" },
     {"Character Map", "gucharmap", "/usr/share/icons/gnome/16x16/apps/accessories-character-map.png" },
     {"Desktop Search", "tracker-needle", "/usr/share/icons/gnome/16x16/actions/system-search.png" },
     {"Disks", "gnome-disks", "/usr/share/icons/hicolor/16x16/apps/gnome-disks.png" },
     {"Files", "nautilus --new-window ", "/usr/share/icons/gnome/16x16/apps/system-file-manager.png" },
     {"Font Viewer", "gnome-font-viewer ", "/usr/share/icons/gnome/16x16/apps/preferences-desktop-font.png" },
     {"GitKraken", "\"gitkraken\"", "/usr/share/pixmaps/gitkraken.png" },
     {"Light Table", "lighttable", "/usr/share/pixmaps/lighttable.png" },
     {"Screenshot", "gnome-screenshot --interactive", "/usr/share/icons/gnome/16x16/apps/applets-screenshooter.png" },
     {"SpeedCrunch", "speedcrunch", "/usr/share/pixmaps/speedcrunch.png" },
     {"gedit", "gedit ", "/usr/share/icons/gnome/16x16/apps/accessories-text-editor.png" },
 }

 local menude7a22a0c94aa64ba2449e520aa20c99 = {
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
 }

 local menu251bd8143891238ecedc306508e29017 = {
     {"Steam", "/usr/bin/steam ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"vitetris", "xterm -e tetris -w 80", "/usr/share/pixmaps/vitetris.xpm" },
 }

 local menud334dfcea59127bedfcdbe0a3ee7f494 = {
     {"ColorGrab", "colorgrab", "/usr/share/icons/hicolor/16x16/apps/colorgrab.png" },
     {"Document Viewer", "evince ", "/usr/share/icons/hicolor/16x16/apps/evince.png" },
     {"GNU Image Manipulation Program", "gimp-2.8 ", "/usr/share/icons/hicolor/16x16/apps/gimp.png" },
     {"Gcolor2", "gcolor2", "///usr/share/pixmaps/gcolor2/icon.png" },
     {"Gpick", "gpick", "/usr/share/icons/hicolor/48x48/apps/gpick.png" },
     {"Image Viewer", "eog ", "/usr/share/icons/hicolor/16x16/apps/eog.png" },
     {"Inkscape", "inkscape ", "/usr/share/icons/hicolor/16x16/apps/inkscape.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"LightZone", "lightzone ", "/usr/share/icons/hicolor/16x16/apps/lightzone.png" },
     {"Pinta", "pinta ", "/usr/share/icons/hicolor/16x16/apps/pinta.png" },
 }

 local menuc8205c7636e728d448c2774e6a4a944b = {
     {"Avahi SSH Server Browser", "/usr/bin/bssh", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Avahi VNC Server Browser", "/usr/bin/bvnc", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Chromium", "chromium ", "/usr/share/icons/hicolor/16x16/apps/chromium.png" },
     {"Empathy", "empathy", "/usr/share/icons/hicolor/16x16/apps/empathy.png" },
     {"Firefox", "firefox ", "/usr/share/icons/hicolor/16x16/apps/firefox.png" },
     {"GitKraken", "\"gitkraken\"", "/usr/share/pixmaps/gitkraken.png" },
     {"Opera", "opera ", "/usr/share/icons/hicolor/16x16/apps/opera.png" },
     {"Steam", "/usr/bin/steam ", "/usr/share/icons/hicolor/16x16/apps/steam.png" },
     {"Telegram Desktop", "telegram -- ", "/usr/share/pixmaps/telegram.png" },
     {"Thunderbird", "thunderbird ", "/usr/share/icons/hicolor/16x16/apps/thunderbird.png" },
     {"Tor-Browser (fr)", "/usr/bin/tor-browser-fr", "///usr/share/pixmaps/tor-browser-fr.png" },
     {"Transmission", "transmission-gtk ", "/usr/share/icons/hicolor/16x16/apps/transmission.png" },
     {"Web", "epiphany ", "/usr/share/icons/gnome/16x16/apps/web-browser.png" },
 }

 local menudf814135652a5a308fea15bff37ea284 = {
     {"Dictionary", "gnome-dictionary", "/usr/share/icons/gnome/16x16/apps/accessories-dictionary.png" },
     {"Document Viewer", "evince ", "/usr/share/icons/hicolor/16x16/apps/evince.png" },
     {"LibreOffice", "libreoffice ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-startcenter.png" },
     {"LibreOffice Base", "libreoffice --base ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-base.png" },
     {"LibreOffice Calc", "libreoffice --calc ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-calc.png" },
     {"LibreOffice Draw", "libreoffice --draw ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-draw.png" },
     {"LibreOffice Impress", "libreoffice --impress ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-impress.png" },
     {"LibreOffice Math", "libreoffice --math ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-math.png" },
     {"LibreOffice Writer", "libreoffice --writer ", "/usr/share/icons/hicolor/16x16/apps/libreoffice-writer.png" },
 }

 local menue6f43c40ab1c07cd29e4e83e4ef6bf85 = {
     {"CMake", "cmake-gui ", "/usr/share/icons/hicolor/32x32/apps/CMakeSetup.png" },
     {"Diffuse Merge Tool", "diffuse -s ", "/usr/share/icons/hicolor/16x16/apps/diffuse.png" },
     {"Eclipse", "eclipse", "/usr/share/icons/hicolor/16x16/apps/eclipse.png" },
     {"Emacs", "emacs ", "/usr/share/icons/hicolor/16x16/apps/emacs.png" },
     {"GitKraken", "\"gitkraken\"", "/usr/share/pixmaps/gitkraken.png" },
     {"IntelliJ IDEA Community Edition", "idea.sh ", "/usr/share/pixmaps/idea.png" },
     {"Light Table", "lighttable", "/usr/share/pixmaps/lighttable.png" },
     {"OpenJDK 1.7.0 Monitoring & Management Console", "_BINDIR_/jconsole"},
     {"OpenJDK 1.7.0 Policy Tool", "_BINDIR_/policytool"},
     {"Qt4 Assistant ", "assistant-qt4", "/usr/share/icons/hicolor/32x32/apps/assistant-qt4.png" },
     {"Qt4 Designer", "designer-qt4", "/usr/share/icons/hicolor/128x128/apps/designer-qt4.png" },
     {"Qt4 Linguist ", "linguist-qt4", "/usr/share/icons/hicolor/16x16/apps/linguist-qt4.png" },
     {"Qt4 QDbusViewer ", "qdbusviewer-qt4", "/usr/share/icons/hicolor/32x32/apps/qdbusviewer-qt4.png" },
 }

 local menu52dd1c847264a75f400961bfb4d1c849 = {
     {"Cheese", "cheese", "/usr/share/icons/hicolor/16x16/apps/cheese.png" },
     {"DeaDBeeF", "deadbeef ", "/usr/share/icons/hicolor/16x16/apps/deadbeef.png" },
     {"PulseAudio Volume Control", "pavucontrol", "/usr/share/icons/hicolor/16x16/apps/multimedia-volume-control.png" },
     {"Qt V4L2 test Utility", "qv4l2", "/usr/share/icons/hicolor/16x16/apps/qv4l2.png" },
     {"VLC media player", "/usr/bin/vlc --started-from-file ", "/usr/share/icons/hicolor/16x16/apps/vlc.png" },
     {"Videos", "totem ", "/usr/share/icons/hicolor/16x16/apps/totem.png" },
     {"ripperX", "ripperX ", "/usr/share/icons/ripperX.xpm" },
 }

 local menuee69799670a33f75d45c57d1d1cd0ab3 = {
     {"Avahi Zeroconf Browser", "/usr/bin/avahi-discover", "/usr/share/icons/gnome/16x16/devices/network-wired.png" },
     {"Color Profile Viewer", "gcm-viewer", "/usr/share/icons/hicolor/16x16/apps/gnome-color-manager.png" },
     {"Disk Usage Analyzer", "baobab", "/usr/share/icons/hicolor/16x16/apps/baobab.png" },
     {"File Manager PCManFM", "pcmanfm ", "/usr/share/icons/gnome/16x16/apps/system-file-manager.png" },
     {"GParted", "/usr/bin/gparted_polkit ", "/usr/share/icons/hicolor/16x16/apps/gparted.png" },
     {"Htop", "xterm -e htop", "/usr/share/pixmaps/htop.png" },
     {"LXTerminal", "lxterminal", "/usr/share/icons/hicolor/128x128/apps/lxterminal.png" },
     {"Oracle VM VirtualBox", "VirtualBox ", "/usr/share/icons/hicolor/16x16/mimetypes/virtualbox.png" },
     {"System Log", "gnome-system-log", "/usr/share/icons/hicolor/16x16/apps/logview.png" },
     {"System Monitor", "gnome-system-monitor", "/usr/share/icons/gnome/16x16/apps/utilities-system-monitor.png" },
     {"Terminal", "gnome-terminal", "/usr/share/icons/gnome/16x16/apps/utilities-terminal.png" },
     {"Terminator", "terminator", "/usr/share/icons/hicolor/16x16/apps/terminator.png" },
     {"UXTerm", "uxterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"XTerm", "xterm", "/usr/share/pixmaps/xterm-color_48x48.xpm" },
     {"dconf Editor", "dconf-editor", "/usr/share/icons/hicolor/16x16/apps/dconf-editor.png" },
 }

xdgmenu = {
    {"Accessories", menu98edb85b00d9527ad5acebe451b3fae6},
    {"Education", menude7a22a0c94aa64ba2449e520aa20c99},
    {"Games", menu251bd8143891238ecedc306508e29017},
    {"Graphics", menud334dfcea59127bedfcdbe0a3ee7f494},
    {"Internet", menuc8205c7636e728d448c2774e6a4a944b},
    {"Office", menudf814135652a5a308fea15bff37ea284},
    {"Programming", menue6f43c40ab1c07cd29e4e83e4ef6bf85},
    {"Sound & Video", menu52dd1c847264a75f400961bfb4d1c849},
    {"System Tools", menuee69799670a33f75d45c57d1d1cd0ab3},
}

