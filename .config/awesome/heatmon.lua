-- local test = proc_temperature()
--local function proc_temperature(textbox_widget, refresh_interval)

   

local output = {}

local cool    = "#7F9F7F"
local puff    = "#ff8700"
local diying  = "#CC9393"
local color   = "#f1f1f1"

module (heatmon.widget)
   
-- Get sensors content
-- Retrieve third line
-- Keeps only the second result
--local handle = io.popen("sensors | sed '3!d' | awk '{ print $2 }'")
--local temp = handle:read("*a")
--handle:close()

local temp = os.execute("sensors | sed '3!d' | awk '{ print $2 }'")
   
naughty.notify({ preset = naughty.config.presets.critical,
                 title = "DEBUG!",
                 text =  temp })




-- Perl regex that remove the floating point and temperature symbols
-- local digit_only_temp = os.execute("sensors | sed '3!d' | awk '{ print $2 }' | perl -pe 's/(\+|°|C|\.[0-9])//g'")

local digit_only_temp = 60

if ( digit_only_temp <= 60 ) then
   color = cool
elseif ( digit_only_temp <= 80 ) then
   color = orange
elseif ( digit_only_temp <= 95 ) then
   color = diying
end

   
table.insert( output,
              "<span color=\""
                 .. color 
                 .. "\">"
                 .. temp
                 .. "</span>" )

return table.concat(output, " ")


